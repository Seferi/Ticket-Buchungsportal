import 'package:bremer_philharmoniker/pages/about_page.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/widgets/karten_bestellen.dart';
import 'package:bremer_philharmoniker/widgets/konzert_item.dart';
import 'package:bremer_philharmoniker/widgets/warenkorbFenster.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'constants/app_assets.dart';
import 'layouts/large_layout.dart';
import 'layouts/medium_layout.dart';
import 'layouts/small_layout.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await AppAssets.preloadSVGs();
  runApp(ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.lightGreen,
        ),
        home: MyHomePage(),
        routes: {
          MyHomePage.routeName: (ctx) => MyHomePage(),
          AboutPage.routeName: (ctx) => AboutPage(),
          WarenkorbFenster.routeName: (ctx) => WarenkorbFenster(),
          KonzertItem.routeName: (ctx) => KonzertItem(),
        });
  }
}

class MyHomePage extends StatefulWidget {
  static const routeName = "/home-page";

  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();
  }

  void initKonzerte(WidgetRef ref) {
    ref.watch(konzertProvider).fetchAndSetKonzerte();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    if (mediaQuery.size.width > 1024) {
      return Consumer(builder: (context, ref, child) {initKonzerte(ref);  return LargeLayout();},);
    }

    if (mediaQuery.size.width > 620) {
      return MediumLayout();
    }

    return SmallLayout();
  }

}
