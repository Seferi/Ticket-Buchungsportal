import 'package:flutter_svg/flutter_svg.dart';

class AppAssets {
  // System UI
  static const logo = "assets/BremerPhilLogo.svg";
  static const evLogo = "BremerPhilEVLogo.gif";

  static Future<void> preloadSVGs() async {
    final assets = [
      // System UI
      logo,
    ];
    for (final asset in assets) {
      await precachePicture(
        ExactAssetPicture(SvgPicture.svgStringDecoder, asset),
        null,
      );
    }
  }
}
