import 'package:bremer_philharmoniker/pages/main_page_body_large_screen.dart';
import 'package:bremer_philharmoniker/widgets/app_bar_brephi.dart';
import 'package:bremer_philharmoniker/widgets/drawer_brephi.dart';
import 'package:flutter/material.dart';

class LargeLayout extends StatelessWidget {
  const LargeLayout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBarBrePhi(),
        drawer: DrawerBrePhi(),
        body: MainPageBodyLargeScreen(),
      ),
    );
  }
}