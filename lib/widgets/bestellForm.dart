import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/models/bestellung.dart';
import 'package:bremer_philharmoniker/providers/warenkorb_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class BestellForm extends ConsumerStatefulWidget {
  const BestellForm({Key? key}) : super(key: key);

  @override
  _BestellFormState createState() => _BestellFormState();
}

class _BestellFormState extends ConsumerState<BestellForm> {
  final _vornameFocusNode = FocusNode();
  final _nachnameFocusNode = FocusNode();
  final _emailFocusNode = FocusNode();
  final _telephoneFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  bool _isLoading = false;
  var _neuBestellung = Bestellung(
    bestellungsId: "",
    //Hier kommt UUID später oder unique key von Firebase
    vorname: "",
    nachname: "",
    email: "",
    telephone: "",
    konzertIdUndkartenAnzahl: {},
    gesamtPreis: 0,
  );

  void _saveForm() {
    final isValid = _form.currentState!.validate();
    if (isValid) {
      _form.currentState!.save();
      setState(() {
        _isLoading = true;
      });
      ref.watch(warenkorbProvider).items.values.forEach((warenkorbItem) {
        _neuBestellung.konzertIdUndkartenAnzahl.putIfAbsent(
            warenkorbItem.konzertId, () => warenkorbItem.kartenAnzahl);
        _neuBestellung.gesamtPreis += warenkorbItem.preis;
      });
      try {
        ref.watch(warenkorbProvider).bestellungHinzufuegen(_neuBestellung);
      } catch (error) {
        showDialog<Null>(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("An error occurred!"),
            content: Text("Something went wrong."),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
                child: Text("Okay"),
              ),
            ],
          ),
        );
      }
      _bestellungErfolgNachricht();
      ref.watch(warenkorbProvider).clearWarenkorb();
    }
    setState(() {
      _isLoading = false;
    });
  }

  void _bestellungErfolgNachricht() {
    showDialog<Null>(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text("Erfolg!"),
        content: Text("Dein bestellung ist erfolgreich abgeschlossen!"),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: Text("Okay"),
          ),
        ],
      ),
    );
  }

  Widget _buildMobileUI() {
    return Container(
        width: MediaQuery.of(context).size.width,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
          elevation: 8.0,
          margin: EdgeInsets.all(10),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Daten zu Ihrer Bestellung \n",
                  style:
                      TextStyles.heading.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text(
                  "Es sind nur mehr wenige Schritte, um Ihre Bestellung abzuschließen: "
                  "Bitte füllen Sie untenstehende Felder vollständig aus. "
                  "Nach Ihrer Bestellung können Sie Ihre Karten einfach selbst ausdrucken oder "
                  "diese digital als PDF auf Ihrem Smartphone zum Konzert mitnehmen.",textAlign: TextAlign.center,
                  style: TextStyles.shortInfo,
                ),
              ),
              Form(
                key: _form,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: "Vorname",
                          border: OutlineInputBorder(),
                          icon: Icon(Icons.person_outlined),
                        ),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_nachnameFocusNode);
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Bitte ein Name eingeben.";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _neuBestellung.vorname = value!;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        focusNode: _nachnameFocusNode,
                        decoration: InputDecoration(
                            labelText: "Nachname",
                            border: OutlineInputBorder(),
                            icon: Icon(Icons.person_outlined)),
                        textInputAction: TextInputAction.next,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context).requestFocus(_emailFocusNode);
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Bitte ein Nachname eingeben.";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _neuBestellung.nachname = value!;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        focusNode: _emailFocusNode,
                        decoration: InputDecoration(
                            labelText: "E-Mail",
                            border: OutlineInputBorder(),
                            icon: Icon(Icons.email_outlined)),
                        textInputAction: TextInputAction.next,
                        keyboardType: TextInputType.emailAddress,
                        onFieldSubmitted: (_) {
                          FocusScope.of(context)
                              .requestFocus(_telephoneFocusNode);
                        },
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Bitte ein e-mail eingeben.";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _neuBestellung.email = value!;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        focusNode: _telephoneFocusNode,
                        decoration: InputDecoration(
                            labelText: "Telephone",
                            border: OutlineInputBorder(),
                            icon: Icon(Icons.phone_outlined)),
                        textInputAction: TextInputAction.done,
                        keyboardType: TextInputType.phone,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Bitte ein telephone eingeben.";
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _neuBestellung.nachname = value!;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: ElevatedButton.icon(
                        onPressed: () {
                          _saveForm();
                        },
                        icon: Icon(Icons.money),
                        label: Text("zahlungspflichtig bestellen"),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery.of(context).size.width < 620
        ? _buildMobileUI()
        : Expanded(
            child: FractionallySizedBox(
              widthFactor: 1.0,
              heightFactor: 1.0,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40),
                ),
                elevation: 8.0,
                margin: EdgeInsets.all(10),
                child: SingleChildScrollView(
                  child: _isLoading
                      ? CircularProgressIndicator()
                      : Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                "Daten zu Ihrer Bestellung \n",
                                style: TextStyles.heading
                                    .copyWith(fontWeight: FontWeight.bold),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Text(
                                "Es sind nur mehr wenige Schritte, um Ihre Bestellung abzuschließen: "
                                "Bitte füllen Sie untenstehende Felder vollständig aus. "
                                "Nach Ihrer Bestellung können Sie Ihre Karten einfach selbst ausdrucken oder "
                                "diese digital als PDF auf Ihrem Smartphone zum Konzert mitnehmen.",
                                style: TextStyles.content,
                              ),
                            ),
                            Form(
                              key: _form,
                              child: SingleChildScrollView(
                                child: Column(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: TextFormField(
                                              decoration: InputDecoration(
                                                labelText: "Vorname",
                                                border: OutlineInputBorder(),
                                                icon:
                                                    Icon(Icons.person_outlined),
                                              ),
                                              textInputAction:
                                                  TextInputAction.next,
                                              onFieldSubmitted: (_) {
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        _nachnameFocusNode);
                                              },
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return "Bitte ein Name eingeben.";
                                                }
                                                return null;
                                              },
                                              onSaved: (value) {
                                                _neuBestellung.vorname = value!;
                                              },
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 20.0),
                                            child: TextFormField(
                                              focusNode: _nachnameFocusNode,
                                              decoration: InputDecoration(
                                                  labelText: "Nachname",
                                                  border: OutlineInputBorder(),
                                                  icon: Icon(
                                                      Icons.person_outlined)),
                                              textInputAction:
                                                  TextInputAction.next,
                                              onFieldSubmitted: (_) {
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        _emailFocusNode);
                                              },
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return "Bitte ein Nachname eingeben.";
                                                }
                                                return null;
                                              },
                                              onSaved: (value) {
                                                _neuBestellung.nachname =
                                                    value!;
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: TextFormField(
                                              focusNode: _emailFocusNode,
                                              decoration: InputDecoration(
                                                  labelText: "E-Mail",
                                                  border: OutlineInputBorder(),
                                                  icon: Icon(
                                                      Icons.email_outlined)),
                                              textInputAction:
                                                  TextInputAction.next,
                                              keyboardType:
                                                  TextInputType.emailAddress,
                                              onFieldSubmitted: (_) {
                                                FocusScope.of(context)
                                                    .requestFocus(
                                                        _telephoneFocusNode);
                                              },
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return "Bitte ein e-mail eingeben.";
                                                }
                                                return null;
                                              },
                                              onSaved: (value) {
                                                _neuBestellung.email = value!;
                                              },
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 20.0),
                                            child: TextFormField(
                                              focusNode: _telephoneFocusNode,
                                              decoration: InputDecoration(
                                                  labelText: "Telephone",
                                                  border: OutlineInputBorder(),
                                                  icon: Icon(
                                                      Icons.phone_outlined)),
                                              textInputAction:
                                                  TextInputAction.done,
                                              keyboardType: TextInputType.phone,
                                              validator: (value) {
                                                if (value!.isEmpty) {
                                                  return "Bitte ein telephone eingeben.";
                                                }
                                                return null;
                                              },
                                              onSaved: (value) {
                                                _neuBestellung.nachname =
                                                    value!;
                                              },
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    ElevatedButton.icon(
                                      onPressed: () {
                                        _saveForm();
                                      },
                                      icon: Icon(Icons.money),
                                      label:
                                          Text("zahlungspflichtig bestellen"),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                ),
              ),
            ),
          );
  }
}
