import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/widgets/konzert_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

//TODO: Die vergangene konzerte nicht zeigen... In KonzertProvider implementieren.

class KonzertList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final _konzerte = ref.watch(konzertProvider);
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return Column(
            children: [
              KonzertListItem(
                konzertId: _konzerte.konzerte[index].id,
                title: _konzerte.konzerte[index].title,
                konzertsaal: _konzerte.konzerte[index].konzertVenue,
                konzertTermin: _konzerte.konzerte[index].konzerttermin,
              ),
            ],
          );
        },
        itemCount: _konzerte.konzerte.length,
      );
    });
  }
}
