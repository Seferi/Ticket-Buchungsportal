import 'package:bremer_philharmoniker/constants/app_assets.dart';
import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/main.dart';
import 'package:bremer_philharmoniker/pages/about_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DrawerBrePhi extends StatelessWidget {
  const DrawerBrePhi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: double.infinity,
            height: MediaQuery.of(context).size.height / 4,
            child: ClipRRect(
              borderRadius: BorderRadius.only(bottomRight: Radius.circular(50)),
              child: Container(
                color: Colors.lightGreen,
                alignment: Alignment.center,
                child: SvgPicture.asset(
                  AppAssets.logo,
                  fit: BoxFit.contain,
                  alignment: Alignment.center,
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.pushNamed(context, MyHomePage.routeName);
                    },
                    icon: Icon(
                      Icons.library_music,
                      color: Colors.black,
                    ),
                    label: Text("Konzerte",
                        style: TextStyle(fontSize: 24, color: Colors.black)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.pushNamed(context, AboutPage.routeName);
                    },
                    icon: Icon(
                      Icons.phone_outlined,
                      color: Colors.black,
                    ),
                    label: Text("Kontakt",
                        style: TextStyle(fontSize: 24, color: Colors.black)),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
