import 'package:bremer_philharmoniker/constants/text_styles.dart';
import 'package:bremer_philharmoniker/providers/konzert_provider.dart';
import 'package:bremer_philharmoniker/providers/warenkorb_provider.dart';
import 'package:bremer_philharmoniker/widgets/warenkorbFenster.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:flutter_add_to_cart_button/flutter_add_to_cart_button.dart';

class KartenBestellen extends StatefulWidget {
  const KartenBestellen({Key? key}) : super(key: key);

  @override
  _KartenBestellenState createState() => _KartenBestellenState();
}

class _KartenBestellenState extends State<KartenBestellen> {
  late int _kartenAnzahl = 1;
  AddToCartButtonStateId stateId = AddToCartButtonStateId.idle;

  Widget _kartenBestellen() {
    return Consumer(
      builder: (context, ref, child) {
        final _konzert = ref.watch(selectedKonzertProvider).state;
        final _warenkorb = ref.watch(warenkorbProvider);
        return SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  "Karten Bestellen",
                  style: TextStyles.heading,
                  textAlign: TextAlign.center,
                ),
              ),
              Divider(),
              Padding(
                padding: EdgeInsets.only(top: 5.0),
                child: Text(
                  _konzert.title,
                  style:
                      TextStyles.content.copyWith(fontWeight: FontWeight.bold),
                  textAlign: TextAlign.left,
                ),
              ),
              Text(
                "${DateFormat("EEE").format(DateTime.parse(_konzert.konzerttermin))} | "
                "${DateFormat("d. MMMM").format(DateTime.parse(_konzert.konzerttermin))} \n "
                "${DateFormat("HH:mm").format(DateTime.parse(_konzert.konzerttermin))} Uhr",
                textAlign: TextAlign.center,
                style: TextStyles.content,
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Container(
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                      color: Colors.white,
                      border: Border.all()),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton<int>(
                        style: TextStyles.content.copyWith(color: Colors.black),
                        elevation: 8,
                        value: _kartenAnzahl,
                        items: [
                          DropdownMenuItem(child: Text("1 Karte"), value: 1),
                          DropdownMenuItem(child: Text("2 Karten"), value: 2),
                          DropdownMenuItem(child: Text("3 Karten"), value: 3),
                          DropdownMenuItem(child: Text("4 Karten"), value: 4),
                          DropdownMenuItem(child: Text("5 Karten"), value: 5),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _kartenAnzahl = value!;
                          });
                        }),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: AddToCartButton(
                  trolley: Image.asset(
                    'assets/ic_cart.png',
                    width: 24,
                    height: 24,
                    color: Colors.black,
                  ),
                  text: Text(
                    'in den Warenkorb',
                    textAlign: TextAlign.center,
                    style: TextStyles.content,
                    maxLines: 1,
                    overflow: TextOverflow.fade,
                  ),
                  check: SizedBox(
                    width: 48,
                    height: 48,
                    child: Icon(
                      Icons.check,
                      color: Colors.black,
                      size: 24,
                    ),
                  ),
                  borderRadius: BorderRadius.circular(24),
                  backgroundColor: Colors.red,
                  onPressed: (id) {
                    if (id == AddToCartButtonStateId.idle) {
                      //handle logic when pressed on idle state button.
                      setState(() {
                        stateId = AddToCartButtonStateId.loading;
                        Future.delayed(Duration(seconds: 2), () {
                          setState(() {
                            stateId = AddToCartButtonStateId.done;
                          });
                          _warenkorb.hinzufuegen(_konzert.id, _konzert.title,
                              _kartenAnzahl, _konzert.einzelKartePreis);
                        });
                      });
                    } else if (id == AddToCartButtonStateId.done) {
                      //handle logic when pressed on done state button.
                      setState(() {
                        stateId = AddToCartButtonStateId.idle;
                      });
                    }
                  },
                  stateId: stateId,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton.icon(
                  onPressed: () {
                    Navigator.of(context).pushNamed(WarenkorbFenster.routeName);
                  },
                  icon: Icon(Icons.shopping_cart),
                  label: Text(
                    "zum Warenkorb",
                    style: TextStyles.content,
                  ),
                  style: ButtonStyle(
                    padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                    backgroundColor: MaterialStateProperty.all(Colors.white),
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        side: BorderSide(style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(24),
                      ),
                    ),
                  ),
                ),
              ),
              /*ElevatedButton.icon(
                onPressed: () {
                  _warenkorb.hinzufuegen(_konzert.id, _konzert.title,
                      _kartenAnzahl, _konzert.einzelKartePreis);
                  print(_warenkorb.items.length);
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text("Karten Hinzugefügt!"),
                      duration: Duration(seconds: 3),
                      action: SnackBarAction(
                        label: "UNDO",
                        onPressed: () {
                          _warenkorb.entfernen(_konzert.id, _kartenAnzahl,
                              _konzert.einzelKartePreis);
                        },
                      ),
                    ),
                  );
                },
                icon: Icon(Icons.add_shopping_cart_outlined),
                label: Text(
                  "in den Warenkorb",
                  style: TextStyles.content,
                ),
                style: ButtonStyle(
                  padding: MaterialStateProperty.all(EdgeInsets.all(20)),
                  backgroundColor: MaterialStateProperty.all(Colors.white),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      side: BorderSide(style: BorderStyle.solid),
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),*/
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Preis pro karte:  € 20",
                  style: TextStyles.shortInfo,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return _kartenBestellen();
  }
}
