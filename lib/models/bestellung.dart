class Bestellung {
  String bestellungsId;
  String vorname;
  String nachname;
  String email;
  String telephone;
  Map<String, int> konzertIdUndkartenAnzahl;
  double gesamtPreis;

  Bestellung({
    required this.bestellungsId,
    required this.vorname,
    required this.nachname,
    required this.email,
    required this.telephone,
    required this.konzertIdUndkartenAnzahl,
    required this.gesamtPreis,
  });
}
