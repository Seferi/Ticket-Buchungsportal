class Konzertsaal {
  late String name;
  late String adresse;            // Ein adresse object wäre besser falls wir die adresse mit maps zeigen wollen...
  late int personenZahl;

  Konzertsaal({
    required this.name,
    required this.adresse,
    required this.personenZahl,
  });

  factory Konzertsaal.fromJson(Map<String, dynamic> parsedJson) {
    final name = parsedJson["name"] as String;
    final addresse = parsedJson["adresse"] as String;
    final personenZahl = parsedJson["personenZahl"] as int;
    return Konzertsaal(name: name, adresse: addresse, personenZahl: personenZahl);
  }
}


